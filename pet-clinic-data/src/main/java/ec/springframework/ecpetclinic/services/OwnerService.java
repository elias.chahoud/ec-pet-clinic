package ec.springframework.ecpetclinic.services;


import ec.springframework.ecpetclinic.model.Owner;

public interface OwnerService extends CrudService<Owner, Long> {

  Owner findByLastName(String lastName);

}
