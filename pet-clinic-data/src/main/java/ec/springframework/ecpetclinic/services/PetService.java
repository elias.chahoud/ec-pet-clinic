package ec.springframework.ecpetclinic.services;

import ec.springframework.ecpetclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {

}
