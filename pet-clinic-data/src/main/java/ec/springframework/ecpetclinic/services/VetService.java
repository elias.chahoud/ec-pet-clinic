package ec.springframework.ecpetclinic.services;

import ec.springframework.ecpetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {

}
