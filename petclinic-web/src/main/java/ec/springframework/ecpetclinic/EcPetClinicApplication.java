package ec.springframework.ecpetclinic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcPetClinicApplication {

  public static void main(String[] args) {
    SpringApplication.run(EcPetClinicApplication.class, args);
  }

}
